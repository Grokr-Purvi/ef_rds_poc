﻿using System;
using Amazon;

namespace EF_with_RDS
{
    public static class Config
    {

		public static string SecretName
		{
			get
			{
				//return Environment.GetEnvironmentVariable("secretName");
				return "dbsecret";


			}
		}
		public static string Region
		{
			get
			{
				//return Environment.GetEnvironmentVariable("region");
				return "us-east-1";
			}
		}
		public static string Db_name
		{
			get
			{
				//return Environment.GetEnvironmentVariable("dbName");	
				return "cproc";
			}
		}
		public static dynamic SecretValue
		{
			get
			{
				return SecretManager.GetSecret();
			}
		}
		public static string Db_host
		{
			get
			{
				return SecretValue.host;
			}
		}
		public static string Db_user
		{
			get
			{
				return SecretValue.user;
			}
		}
		public static string Db_pass
		{
			get
			{
				return SecretValue.pass;
			}
		}
	}
}
