﻿using System;
using EF_with_RDS.Models;

namespace EF_with_RDS
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            using (var db = new cprocContext())
            {
                // Creating a new department and saving it to the database
                var newDept = new CliDriver();
/*                newDept. = 60;
                newDept.Dname = "Development";
                newDept.Loc = "Houston";
                db.Dept.Add(newDept);
                var count = db.SaveChanges();
                Console.WriteLine("{0} records saved to database", count);*/

                // Retrieving and displaying data
                Console.WriteLine();
                Console.WriteLine("All records in cli_driver in the database:");
                foreach (var driver in db.CliDrivers)
                {
                    Console.WriteLine("{0} | {1}", driver.Batchid, driver.Qpcidnum);
                }
            }
        }
    }
}
