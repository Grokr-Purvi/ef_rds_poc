﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EF_with_RDS.Models
{
    public partial class CliDriver
    {
        public int Batchid { get; set; }
        public string Qpcidnum { get; set; }
        public short Qpcdrvno { get; set; }
        public string Drvid { get; set; }
        public char Drvtypecd { get; set; }
        public string Nameid { get; set; }
        public string Ssnid { get; set; }
        public string Dlid { get; set; }
        public string Emailid { get; set; }
        public char Gendercd { get; set; }
        public string Relationshipcd { get; set; }
        public DateTime? Dob { get; set; }
        public short? Age { get; set; }
        public char Maritalstatuscd { get; set; }
        public short? Isstudentaway { get; set; }
        public string Occupdesc { get; set; }
        public short? Yrsdriving { get; set; }
    }
}
