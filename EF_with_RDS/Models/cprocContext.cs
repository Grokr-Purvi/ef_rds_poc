﻿using System;
using Microsoft.EntityFrameworkCore;
using static EF_with_RDS.Config;

#nullable disable

namespace EF_with_RDS.Models
{
    public partial class cprocContext : DbContext
    {
        public cprocContext()
        {
        }

        public cprocContext(DbContextOptions<cprocContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CliDriver> CliDrivers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseNpgsql("Host=" + Db_host +";Database="+ Db_name + ";Username=" + Db_user + ";Password=" + Db_pass );
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "en_US.UTF-8");

            modelBuilder.Entity<CliDriver>(entity =>
            {
                entity.HasKey(e => new { e.Batchid, e.Qpcidnum, e.Qpcdrvno })
                    .HasName("cli_driver_pkey");

                entity.ToTable("cli_driver");

                entity.Property(e => e.Batchid).HasColumnName("batchid");

                entity.Property(e => e.Qpcidnum)
                    .HasMaxLength(35)
                    .HasColumnName("qpcidnum");

                entity.Property(e => e.Qpcdrvno).HasColumnName("qpcdrvno");

                entity.Property(e => e.Age).HasColumnName("age");

                entity.Property(e => e.Dlid)
                    .HasMaxLength(36)
                    .HasColumnName("dlid")
                    .IsFixedLength(true);

                entity.Property(e => e.Dob)
                    .HasPrecision(3)
                    .HasColumnName("dob");

                entity.Property(e => e.Drvid)
                    .HasMaxLength(12)
                    .HasColumnName("drvid");

                entity.Property(e => e.Drvtypecd)
                    .HasMaxLength(1)
                    .HasColumnName("drvtypecd");

                entity.Property(e => e.Emailid)
                    .HasMaxLength(36)
                    .HasColumnName("emailid")
                    .IsFixedLength(true);

                entity.Property(e => e.Gendercd)
                    .HasMaxLength(1)
                    .HasColumnName("gendercd");

                entity.Property(e => e.Isstudentaway).HasColumnName("isstudentaway");

                entity.Property(e => e.Maritalstatuscd)
                    .HasMaxLength(1)
                    .HasColumnName("maritalstatuscd");

                entity.Property(e => e.Nameid)
                    .HasMaxLength(36)
                    .HasColumnName("nameid")
                    .IsFixedLength(true);

                entity.Property(e => e.Occupdesc)
                    .HasMaxLength(50)
                    .HasColumnName("occupdesc");

                entity.Property(e => e.Relationshipcd)
                    .HasMaxLength(2)
                    .HasColumnName("relationshipcd")
                    .IsFixedLength(true);

                entity.Property(e => e.Ssnid)
                    .HasMaxLength(36)
                    .HasColumnName("ssnid")
                    .IsFixedLength(true);

                entity.Property(e => e.Yrsdriving).HasColumnName("yrsdriving");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
